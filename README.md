# How to install BrivoMobilePassSDK
## Cocoapods

In your pod file, after 
```
  use_frameworks!
```
You should add
```
    pod 'BrivoMobilePassSDK', :source => 'https://bitbucket.org/brivoinc/mobile-sdk-framework-spec.git'
```
After adding the line above, your pod file should look something like this:
```
target 'YourApplicationName' do
  use_frameworks!

  pod 'BrivoMobilePassSDK', :source => 'https://bitbucket.org/brivoinc/mobile-sdk-framework-spec.git'
end
```
## Manual installation
##### For manual installation, download the framework from the latest tag.

## Documentation
##### The documentation for the sdk and the license can be found at the latest tag. 